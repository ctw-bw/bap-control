# BAP Control Library #

This repository contains a Simulink library with the blocks to control the BAP set-up over Simulink Real-Time. The BAP was finished in 2012, its control done in MATLAB XPC. In 2021 we are making an effort to revive the set-up, using MATLAB Simulink Real-Time.

See Confluence for more info on SLRT: https://be.et.utwente.nl/confluence/x/HoAXAg

## Getting Started ##

### Running the examples ###

 1. Clone this repository inclusing submodules: `git clone --recurse-submodules <url>`
     * If you forgot, you can checkout the submodules later with: `git submodule update --init`
 2. Open an `examples/<project>` directory in MATLAB
 3. Run the `<project>_init.m` script to create the necessary variables and add the right libraries to your path
 4. Open `<project>.slx` and compile and upload it to the target

### Create your own project ###

 1. Create/clone your own repository
 2. Add the control library as a submodule: `git submodule add ../bap-control.git`
 3. Open the library and drag the input-output block into your own model
     * In the instance in your model will stay linked to the library
 4. Don't forget to create a `<model>_init.m` script to make it easy to get the preparations for your model

You will likely need at least these paths:

```
addpath('bap-control/slrt-humusoft-library/humusoftlib');
addpath('bap-control/slrt-ni-library/nilib');
addpath('bap-control');
```

## Humusoft and NI ##

Support for the MF624 PCI card was dropped by Simulink. Instead the [Humosoft Library](../../../slrt-humusoft-library) is used. See the library repository for more information. The same goes for [NI Library](../../../slrt-ni-library)
